import { Routes } from "@angular/router";

import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { NavbarComponent } from "./components/navbar/navbar.component";

export const routes: Routes = [
    {path:'login',component: LoginComponent},
    {path:'home',component: HomeComponent},
    {path:'navbar', component: NavbarComponent},
    {path: '', pathMatch:'full' ,redirectTo:'login'},
    //{path:'**', pathMatch:'full',redirectTo:'home'}
];